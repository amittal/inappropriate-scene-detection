import os
import pandas as pd


def clean_and_create(type):
	stats = []

	path = "overflow/dissertation/Data/"+type+""

	for i in os.listdir(path):
		if(i[0] != '.'):
			m = list(os.stat(path+'/'+i))
			if m[6] == 0:
				os.remove(path+'/'+i)
			else:
				m.append(i)
				stats.append(m)
		

	df = pd.DataFrame(stats)

	print(df.head)

	df.columns = ['mode', 'ino', 'dev', 'nlink', 'uid', 'gid', 'size', 'atime', 'mtime', 'ctime', 'filename']

	df.to_csv("overflow/dissertation/Data/metadata_file_"+type+".csv")
    
clean_and_create('train')
clean_and_create('test')
clean_and_create('val')