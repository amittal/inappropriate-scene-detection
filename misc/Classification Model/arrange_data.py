import os
import shutil

src = "overflow/dissertation/Data/"

def arrange(type):
    
    try:
        shutil.rmtree(src+type+'/'+'0')
    except:
        print(src+type+'/'+'0',' does not exist')
        
    try:
        shutil.rmtree(src+type+'/'+'1')
    except:
        print(src+type+'/'+'1',' does not exist')    

    l = os.listdir(src+type)

    l1 = [i for i in l if i[0] == '0']
    l2 = [i for i in l if i[0] == '1']
    
    os.mkdir(src+type+'/'+'0')
    os.mkdir(src+type+'/'+'1')
    
    for i in l1:
        shutil.move(src+type+'/'+i,src+type+'/0/'+i,)
        
    for i in l2:
        shutil.move(src+type+'/'+i,src+type+'/1/'+i,)
    

arrange('train')
arrange('test')
arrange('val')