from PIL import Image
import os
import pandas as pd
from multiprocessing import Pool
import glob
import fnmatch
import random
import datetime
import os.path

import zipfile
import shutil

### Unzip data
dest = 'overflow/dissertation/Data/bepaudi'
src = 'overflow/dissertation/Data/NudeNet_Classifier_train_data_x320.zip'

try:
    shutil.rmtree(dest)
    print("Main folder removed\n")
except:
    print("Folder not present")    
    
os.mkdir(dest)
print("Main folder added\n")

with zipfile.ZipFile(src, 'r') as zip_ref:
    zip_ref.extractall(dest)
    
print("Main folder Ready for Cleaning\n")
    

### Clean Data
src = dest

l = []
for root, dirnames, filenames in os.walk(src):
    for filename in fnmatch.filter(filenames, '*.*'):
        if filename[0] != '.':
            l.append(os.path.join(root, filename))
        else:
            os.remove(os.path.join(root, filename))

df = pd.DataFrame(l)
df.to_csv("overflow/dissertation/Data/all_unzipped_bepaudi_files.csv")

print("Paths list ready\n")

def clean_data(name):
    a = name.lower().split('.')[:-1]
    a = '.'.join(a).replace('.jpg','')
    out = a+'_proc.jpg'
    try:
        im = Image.open(name)
        out = im.resize((256, 256))
        out.save(out)
    except:
        print("issue 1 with : ",name)
    
    os.remove(name)
    
    if os.path.isfile(out):
        m = list(os.stat(out))
        m.append(out)
        return m
        
pool = Pool()
stats = pool.map(clean_data,l)

df = pd.DataFrame(stats)
df.columns = ['mode', 'ino', 'dev', 'nlink', 'uid', 'gid', 'size', 'atime', 'mtime', 'ctime', 'filename']
df.to_csv("metadata_file_bepaudi.csv")