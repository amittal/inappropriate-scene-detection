import zipfile
import shutil
import os

dest = 'overflow/dissertation/Data/bepaudi'
src = 'overflow/dissertation/Data/NudeNet_Classifier_train_data_x320.zip'

try:
    shutil.rmtree(dest)
except:
    print("Folder not present")    
    
os.mkdir(dest)
    
with zipfile.ZipFile(src, 'r') as zip_ref:
    zip_ref.extractall(dest)