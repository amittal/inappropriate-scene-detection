import datetime

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.callbacks import TensorBoard, LearningRateScheduler
AUTOTUNE = tf.data.experimental.AUTOTUNE

import pandas as pd
import numpy as np
import glob
import os

import resnet

NUM_GPUS = 1
BS_PER_GPU = 128
NUM_EPOCHS = 100

HEIGHT = 32
WIDTH = 32
NUM_CHANNELS = 3
NUM_CLASSES = 2

src = "overflow/dissertation/ModelTest/"
src2 = "overflow/dissertation/Data/"

list_ds = tf.data.Dataset.list_files(src2+"train/*.jpeg")
list_ds2 = tf.data.Dataset.list_files(src2+"test/*.jpeg")

# for f in list_ds.take(5):
#   print(f.numpy())

def get_label(file_path):
  parts = tf.strings.split(file_path, os.path.sep)
  return tf.strings.to_number(tf.strings.split(parts[-1],"_")[0], out_type=tf.dtypes.float32)
 
def decode_img(img):
  # convert the compressed string to a 3D uint8 tensor
  img = tf.image.decode_jpeg(img, channels=3)
  # Use `convert_image_dtype` to convert to floats in the [0,1] range.
  img = tf.image.convert_image_dtype(img, tf.float32)
  # resize the image to the desired size.
  return tf.image.resize(img, [WIDTH, HEIGHT])
  
def process_path(file_path):
  label = get_label(file_path)
  # load the raw data from the file as a string
  img = tf.io.read_file(file_path)
  img = decode_img(img)
  return img, label
  
train_dataset = list_ds.map(process_path, num_parallel_calls=AUTOTUNE)
test_dataset = list_ds2.map(process_path, num_parallel_calls=AUTOTUNE)

for image, label in train_dataset.take(1):
  print("Image shape: ", image.numpy().shape)
  print("Label: ", label.numpy())
                                                     

NUM_TRAIN_SAMPLES = len(glob.glob(src2 + "train/*.jpg"))

BASE_LEARNING_RATE = 0.1
LR_SCHEDULE = [(0.1, 30), (0.01, 45)]


def preprocess(x, y):
  print("Shape of image - ",x.shape)
  x = tf.image.per_image_standardization(x)
  return x, y


def augmentation(x, y):
    x = tf.image.resize_with_crop_or_pad(
        x, HEIGHT + 8, WIDTH + 8)
    x = tf.image.random_crop(x, [HEIGHT, WIDTH, NUM_CHANNELS])
    x = tf.image.random_flip_left_right(x)
    return x, y	


def schedule(epoch):
  initial_learning_rate = BASE_LEARNING_RATE * BS_PER_GPU / 128
  learning_rate = initial_learning_rate
  for mult, start_epoch in LR_SCHEDULE:
    if epoch >= start_epoch:
      learning_rate = initial_learning_rate * mult
    else:
      break
  tf.summary.scalar('learning rate', data=learning_rate, step=epoch)
  return learning_rate


# (x,y), (x_test, y_test) = keras.datasets.cifar10.load_data()


tf.random.set_seed(22)
train_dataset = train_dataset.map(augmentation).map(preprocess).batch(BS_PER_GPU * NUM_GPUS, drop_remainder=True) # .shuffle(NUM_TRAIN_SAMPLES)
test_dataset = test_dataset.map(preprocess).batch(BS_PER_GPU * NUM_GPUS, drop_remainder=True)

input_shape = (WIDTH, HEIGHT, 3)
img_input = tf.keras.layers.Input(shape=input_shape)
opt = keras.optimizers.SGD(learning_rate=0.1, momentum=0.9)

if NUM_GPUS == 1:
    model = resnet.resnet56(img_input=img_input, classes=NUM_CLASSES)
    model.compile(
              optimizer=opt,
              loss='binary_crossentropy',
              metrics=['accuracy'])
else:
    mirrored_strategy = tf.distribute.MirroredStrategy()
    with mirrored_strategy.scope():
      model = resnet.resnet56(img_input=img_input, classes=NUM_CLASSES)
      model.compile(
                optimizer=opt,
                loss='binary_crossentropy',
                metrics=['accuracy'])  

log_dir=src+"logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
file_writer = tf.summary.create_file_writer(log_dir + "/metrics")
file_writer.set_as_default()
tensorboard_callback = TensorBoard(
  log_dir=log_dir,
  update_freq='batch',
  histogram_freq=1)

lr_schedule_callback = LearningRateScheduler(schedule)

model.fit(train_dataset,
          epochs=NUM_EPOCHS,
          validation_data=test_dataset,
          validation_freq=1,
          callbacks=[tensorboard_callback, lr_schedule_callback]
          )
model.evaluate(test_dataset)

model.save(src + 'model.h5')

new_model = keras.models.load_model(src + 'model.h5')
 
new_model.evaluate(test_dataset)