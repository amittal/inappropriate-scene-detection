
from PIL import Image
import os
from multiprocessing import Pool
import glob
import fnmatch
import os.path

### Unzip data
dest = 'overflow/dissertation/Data/bepaudi'
# src = 'overflow/dissertation/Data/NudeNet_Classifier_train_data_x320.zip'


for root, dirnames, filenames in os.walk(dest):
    for filename in fnmatch.filter(filenames, '*.*'):
        path = os.path.join(root, filename)
        try:
            im = Image.open(path)
        except:
            os.remove(path)
            