import tensorflow as tf
import pandas as pd
import numpy as np
import os

source_folder = 'overflow/dissertation/Data/bepaudi/NudeNet_Classifier_train_data_x320/nude_sexy_safe_v1_x320/'
result_file = 'overflow/dissertation/ModelTest/data_cleaner_result.csv'
IMG_HEIGHT = 256
IMG_WIDTH = 256

def decode_img(img):
    img = tf.image.decode_png(img, channels=3)
    img = tf.image.convert_image_dtype(img, tf.float32)
    return tf.image.resize(img, [IMG_HEIGHT, IMG_WIDTH])

## Read file if exists
if os.path.isfile(result_file):
    df = pd.read_csv(result_file)
else:
    df = pd.DataFrame(columns=['image path','file status'])

## Remaining files to work on
file_list = [os.path.join(path, name) for path, subdirs, files in os.walk(source_folder) for name in files]
file_list2 = list(set(file_list) - set(list(df['image path'])))

l = []
cnt = 1 
l2 = []

for image_path in file_list2:
    cnt += 1
    l2.append(image_path)
    if cnt%5000 == 0:
        try:
            for f in tf.data.Dataset.list_files(l2):
                img = tf.io.read_file(f)
                img = decode_img(img)
            l3 = [[item,'Good'] for item in l2]
        except:
            l3 = [[item,'Bad'] for item in l2]
        l2 = []
        df2 = pd.DataFrame(l3, columns = ['image path','file status'])
        df = df.append(df2)
        df.to_csv(result_file, index = False)