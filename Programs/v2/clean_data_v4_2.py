import tensorflow as tf
import pandas as pd
import numpy as np
import os

source_folder = 'overflow/dissertation/Data/bepaudi/NudeNet_Classifier_train_data_x320/nude_sexy_safe_v1_x320/'
result_file = 'overflow/dissertation/ModelTest/data_cleaner_result.csv'

result_file2 = 'overflow/dissertation/ModelTest/data_cleaner_result_bad_files.csv'

IMG_HEIGHT = 256
IMG_WIDTH = 256

def decode_img(img):
    img = tf.image.decode_png(img, channels=3)
    img = tf.image.convert_image_dtype(img, tf.float32)
    return tf.image.resize(img, [IMG_HEIGHT, IMG_WIDTH])

## Read file if exists
if os.path.isfile(result_file):
    df = pd.read_csv(result_file)
else:
    df = pd.DataFrame(columns=['image path','file status'])

## Remaining files to work on

file_list2 = list(df[df['file status'] == 'Bad']['image path'])

l = []
cnt = 1 
l2 = {}

for image_path in file_list2:
    try:
        for f in tf.data.Dataset.list_files([image_path]):
            img = tf.io.read_file(f)
            img = decode_img(img)
        # l2[image_path] = 'Good'
    except:
        l2[image_path] = 'Bad'

df2 = pd.DataFrame(l2.items(), columns = ['image path','file status'])

df2.to_csv(result_file2, index = False)