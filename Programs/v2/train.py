import argparse
import pandas as pd
import numpy as np
import datetime

import tensorflow as tf
AUTOTUNE = tf.data.experimental.AUTOTUNE

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import Callback, EarlyStopping, ModelCheckpoint
from tensorflow.keras.utils import plot_model
from tensorflow.keras.models import load_model

from tensorflow.keras.applications import InceptionResNetV2, NASNetLarge, ResNet50, Xception, MobileNetV2
from efficientnet import *

backend.set_image_data_format('channels_last')
backend.set_learning_phase(1)

############################## Creating arguement parser for easy execution

parser = argparse.ArgumentParser(description='Prepare Data for Training')
parser.add_argument('-model','--model',help="Model architecture to be executed [EfficientNetB0,EfficientNetB5,EfficientNetB7,InceptionResNetV2,NASNetLarge,ResNet50,MobileNetV2,Xception]",type=str)
parser.add_argument('-batch_size','--batch_size',help="Batch size for images to be read by the model (must be chosen with respect to model and 1080 GTX V memory)",type=int)
parser.add_argument('-model_output_path','--model_output_path',help="Destination directory where the model weights and plot will be saved",type=str)
parser.add_argument('-data_src','--data_src',help="Source directory where the data is saved in the format training-validation-testing",type=str)
parser.add_argument('-mode','--mode',help="Mode could be train or eval",type=str)

args = parser.parse_args()

if args.model is None:
    print("Please specify the Model architecture")
    exit(1)
    
if args.batch_size is None:
    print("Please specify batch size")
    exit(1)
    
if args.model_output_path is None:
    print("Please specify the model output path")
    exit(1)
    
if args.data_src is None:
    print("Please specify the data src")
    exit(1)
    
if args.mode is None:
    print("Please specify the mode")
    exit(1)

################################ Define model #########################################

### 1. EfficientNetB0
### 2. EfficientNetB5
### 3. EfficientNetB7
### 4. InceptionResNetV2
### 5. NASNetLarge
### 6. ResNet50
### 7. ResNet152
### 8. Xception

model_name = args.model
batch_size = args.batch_size
src = args.model_output_path
src2 = args.data_src

weight_saver_batches = 1000

model_save_path = src + model_name + "_model.h5"
    
print('Model selected - ',model_name)
print('Batch size set to - ',batch_size)
print('Model saved at - ',src)
print('Data picked from - ',src2)
print('Weigts of the model saved after ',weight_saver_batches,' batches')

#######################################################################################################


if model_name == "EfficientNetB5":
    model = EfficientNetB5(
        include_top=True,
        weights=None,
        input_tensor=None,
        input_shape=(256,256,3),
        pooling=None,
        classes=2
    )
elif model_name == "EfficientNetB0":
    model = EfficientNetB0(
        include_top=True,
        weights=None,
        input_tensor=None,
        input_shape=(256,256,3),
        pooling=None,
        classes=2
    )
elif model_name == "EfficientNetB7":
    model = EfficientNetB7(
        include_top=True,
        weights=None,
        input_tensor=None,
        input_shape=(256,256,3),
        pooling=None,
        classes=2
    )
elif model_name == "EfficientNetB4":
    model = EfficientNetB4(
        include_top=True,
        weights=None,
        input_tensor=None,
        input_shape=(256,256,3),
        pooling=None,
        classes=2
    )
elif model_name == "ResNet50":
    model = ResNet50(
        include_top=True,
        weights=None,
        input_tensor=None,
        input_shape=(256,256,3),
        pooling=None,
        classes=2
    )
elif model_name == "InceptionResNetV2":
    model = InceptionResNetV2(
        include_top=True,
        weights=None,
        input_tensor=None,
        input_shape=(256,256,3),
        pooling=None,
        classes=2,
        classifier_activation="softmax"
    )
elif model_name == "NASNetLarge":
    model = NASNetLarge(
        input_shape=(256,256,3),
        include_top=True,
        weights=None,
        input_tensor=None,
        pooling=None,
        classes=2,
    )
elif model_name == "Xception":
    model = Xception(
        include_top=True,
        weights=None,
        input_tensor=None,
        input_shape=(256,256,3),
        pooling=None,
        classes=2,
        classifier_activation="softmax"
    )
elif model_name == "MobileNetV2":
    model = MobileNetV2(
        include_top=True,
        weights=None,
        input_tensor=None,
        input_shape=(256,256,3),
        pooling=None,
        classes=2,
        classifier_activation="softmax"
    )


if os.path.isfile(model_save_path):
    model.load_weights(model_save_path)
    print('Model weights already present. Picking up from where the program left last.')
    
############### Loading Data #################
if args.mode.lower() == 'train':
    train_datagen = ImageDataGenerator(
            rescale = 1./255)
            # shear_range = 0.2,
            # zoom_range = 0.2,
            # horizontal_flip = True)

    train_generator = train_datagen.flow_from_directory(
            src2+'training',  # this is the target directory
            target_size=(256,256),  # all images will be resized to this size
            batch_size=batch_size,
            class_mode='categorical')  # since we use binary_crossentropy loss, we need binary labels
    
    test_datagen = ImageDataGenerator(rescale=1./255)
    
    test_generator = test_datagen.flow_from_directory(
            src2+'testing',
            target_size=(256,256),
            batch_size=batch_size,
            class_mode='categorical',
            shuffle = False)

validation_datagen = ImageDataGenerator(rescale=1./255)
validation_generator = validation_datagen.flow_from_directory(
        src2+'validation',
        target_size=(256,256),
        batch_size=batch_size,
        class_mode='categorical',
        shuffle = False
        )

############## Callbacks

class WeightsSaver(Callback):
    def __init__(self, N):
        self.N = N
        self.batch = 0

    def on_batch_end(self, batch, logs={}):
        if self.batch % self.N == 0:
            self.model.save(model_save_path)
        self.batch += 1

ws = WeightsSaver(weight_saver_batches)
es = EarlyStopping(monitor='val_accuracy', verbose=1, mode='auto')
mc = ModelCheckpoint(model_save_path, monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)

############ Removing truncated image errors

from PIL import ImageFile, Image
ImageFile.LOAD_TRUNCATED_IMAGES = True

############ Compile model

model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

if args.mode.lower() == 'train':
    print(model.summary())
    STEP_SIZE_TRAIN=train_generator.n//train_generator.batch_size

STEP_SIZE_VALID=validation_generator.n//validation_generator.batch_size

############ Evaluate model

x = model.evaluate_generator(generator=validation_generator,steps=STEP_SIZE_VALID)
print("evaluation result - ",x)

############ Plot model

plot_model(model, to_file=src+model_name+'_model_plot.png')

############ Fit model

if args.mode.lower() == 'train':
    model.fit_generator(generator=train_generator,
                        steps_per_epoch=STEP_SIZE_TRAIN,
                        validation_data=test_generator,
                        validation_steps=STEP_SIZE_VALID,
                        epochs=100,
                        callbacks=[es,mc,ws]
    )

############ Evaluate model

x = model.evaluate_generator(generator=validation_generator,steps=STEP_SIZE_VALID)
print("evaluation result - ",x)

start_ts = datetime.datetime.now()
Y_pred = model.predict_generator(validation_generator, steps=STEP_SIZE_VALID)
end_ts = datetime.datetime.now()
y_pred = np.argmax(Y_pred, axis=1)
y_true = validation_generator.classes
y_true = y_true[:len(y_pred)]
df = pd.DataFrame(data = {"y_true":y_true, "y_pred":y_pred, "Y_pred_class_0":[item[0] for item in Y_pred[:len(y_pred)]], "Y_pred_class_1":[item[1] for item in Y_pred[:len(y_pred)]], "filenames":validation_generator.filenames[:len(y_pred)]})

df["y_true2"] = df["filenames"].map(lambda x: x[0])
df["Result"] = np.where(df["y_true2"].astype('int32') == df["y_pred"].astype('int32'), 1, 0)
df['start_ts'] = start_ts
df['end_ts'] = end_ts

print('Accuracy with predict function - ',np.average(df["Result"]))
df.to_csv(src+model_name+"_eval_results.csv")