import zipfile
import shutil
import os
from PIL import Image
from multiprocessing import Pool
import fnmatch
import argparse
import pandas as pd

### Creating arguement parser for easy execution
parser = argparse.ArgumentParser(description='Prepare Data for Training')
parser.add_argument('-src_dir','--Src',help="Source directory where the github project is cloned",type=str)
parser.add_argument('-data','--Data',help="Path for data as zip file requested from amittal@tcd.ie",type=str)
parser.add_argument('-password','--Password',help="Password for data as zip file requested from amittal@tcd.ie",type=str)
args = parser.parse_args()

if args.Src is None:
    print("Please specify the source directory i.e. the path to cloned github project")
    exit(1)
    
if args.Data is None:
    print("Please specify the path for Zip file for training data requested from amittal@tcd.ie")
    exit(1)
    
if args.Password is None:
    print("Please specify the password for Zip file for training data requested from amittal@tcd.ie")
    exit(1)

src = args.Src

### Download Data

data_dir = src + "Data"
zip_file = args.Data

try:
    shutil.rmtree(data_dir)
except:
    pass

os.mkdir(data_dir)

os.system("cp " + zip_file + " " + data_dir)

### Unzip Data
dest = src + "Data/unzipped_data"
password = args.Password

try:
    shutil.rmtree(dest)
except:
    print("Folder not present")    
    
os.mkdir(dest)
    
with zipfile.ZipFile(data_dir + "/Data.zip", 'r') as zip_ref:
    zip_ref.extractall(dest) # , pwd = password.encode()
    
os.remove(data_dir + "/Data.zip")

### Delete Currup files
l = []
for root, dirnames, filenames in os.walk(dest):
    for filename in fnmatch.filter(filenames, '*.*'):
        l.append(os.path.join(root, filename))

def file_check(path):
    result = 1
    try:
        im = Image.open(path)
    except:
        os.remove(path)
        result = 0
    return [path,result]
            
pool = Pool()
l2 = pool.map(file_check,l)

### Save Metadata
df = pd.DataFrame(l2)
df.columns = ['path','file_ok']
df.to_csv(dest+"/filecheck.csv")

### Rearrange data for binary classification
for type in ['training','testing','validation']:
    
    os.system("mv -v " + dest + "/NudeNet_Classifier_train_data_x320/nude_sexy_safe_v1_x320/" + type + "/sexy/* "+ dest +"/NudeNet_Classifier_train_data_x320/nude_sexy_safe_v1_x320/" + type + "/nude/")
    
    os.system("rm -r " + dest + "/NudeNet_Classifier_train_data_x320/nude_sexy_safe_v1_x320/" + type + "/sexy/")