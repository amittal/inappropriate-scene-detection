import sys
sys.path.append("tensorflow_p36/lib/python3.6/site-packages/tensorflow/models")
sys.path.append("tensorflow_p36/lib/python3.6/site-packages/tensorflow/models/official/nlp/bert")

from tensorflow_examples.lite.model_maker.core.data_util.image_dataloader import ImageClassifierDataLoader
from tensorflow_examples.lite.model_maker.core.task import image_classifier
from tensorflow_examples.lite.model_maker.core.task.model_spec import (mobilenet_v2_spec,
        efficientnet_lite0_spec,
        efficientnet_lite1_spec,
        efficientnet_lite2_spec,
        efficientnet_lite3_spec,
        efficientnet_lite4_spec,
        resnet_50_spec,
        )
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime
import os
import argparse
import imghdr

gpu_options = tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction = 0.7)
sess = tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(gpu_options=gpu_options))

############# Arguements
parser = argparse.ArgumentParser(description='Prepare Data for Training')
parser.add_argument('-model','--model',help="Model architecture to be executed [mobilenet_v2,efficientnet_lite0,efficientnet_lite1,efficientnet_lite2,efficientnet_lite3,efficientnet_lite4,resnet_50]",type=str)
parser.add_argument('-model_output_path','--model_output_path',help="Destination directory where the model weights and plot will be saved",type=str)
parser.add_argument('-data_src','--data_src',help="Source directory where the data is saved in the format training-validation-testing",type=str)
parser.add_argument('-mode','--mode',help="Mode could be train or eval",type=str)
parser.add_argument('-epochs','--epochs',help="Number of epochs to train for. Add random value for eval mode",type=int)
parser.add_argument('-batch_size','--batch_size',help="Batch size for images to be read by the model (must be chosen with respect to model and 1080 GTX V memory)",type=int)

args = parser.parse_args()

if args.model is None:
    print("Please specify the Model architecture")
    exit(1)
    
if args.model_output_path is None:
    print("Please specify the model output path")
    exit(1)
    
if args.data_src is None:
    print("Please specify the data src")
    exit(1)
    
if args.mode is None:
    print("Please specify the mode")
    exit(1)
    
if args.epochs is None:
    print("Please specify the number of epochs")
    exit(1)

if args.batch_size is None:
    print("Please specify batch size")
    exit(1)
    
""" 

cmd script

python train_lite.py -model  -model_output_path  -data_src  -mode  -epochs -batch_size

"""

############## Config
model_name = args.model
src = args.model_output_path
src2 = args.data_src
epochs = args.epochs
batch_size = args.batch_size
mode = args.mode.lower()

print('Model selected - ',model_name)
print('Model saved at - ',src)
print('Data picked from - ',src2)
print('Epochs -  ',epochs)
print('mode = ',mode)

############# Set mod spec

if model_name+'_spec' == 'mobilenet_v2_spec':
    mod_spec = mobilenet_v2_spec
elif model_name+'_spec' == 'efficientnet_lite0_spec':
    mod_spec = efficientnet_lite0_spec
elif model_name+'_spec' == 'efficientnet_lite1_spec':
    mod_spec = efficientnet_lite1_spec
elif model_name+'_spec' == 'efficientnet_lite2_spec':
    mod_spec = efficientnet_lite2_spec
elif model_name+'_spec' == 'efficientnet_lite3_spec':
    mod_spec = efficientnet_lite3_spec
elif model_name+'_spec' == 'efficientnet_lite4_spec':
    mod_spec = efficientnet_lite4_spec
elif model_name+'_spec' == 'resnet_50_spec':
    mod_spec = resnet_50_spec
    
############# Read Data
train_data = ImageClassifierDataLoader.from_folder(src2 + 'training')
validation_data = ImageClassifierDataLoader.from_folder(src2 + 'validation', shuffle = False)

if mode == 'train':
    ############# train model
    model = image_classifier.create(train_data, model_spec = mod_spec, epochs=epochs, validation_data=validation_data,train_whole_model=True, batch_size=batch_size)

    ############# Save Model
    model.export(export_dir = src, tflite_filename = model_name + '.tflite', label_filename = model_name + 'labels.txt')
    print('Saved model ------------------------------------ Saved model')

############# Check model accuracy
# Read TensorFlow Lite model from TensorFlow Lite file.
with tf.io.gfile.GFile(src + model_name + '.tflite', 'rb') as f:
  model_content = f.read()

# Read label names from label file.
with tf.io.gfile.GFile(src + model_name + 'labels.txt', 'r') as f:
  label_names = f.read().split('\n')

# Initialze TensorFlow Lite inpterpreter.
interpreter = tf.lite.Interpreter(model_content=model_content)
interpreter.allocate_tensors()
input_index = interpreter.get_input_details()[0]['index']
output = interpreter.tensor(interpreter.get_output_details()[0]["index"])

# Run predictions on each test image data and calculate accuracy.

def load_image(path):
  """Loads image."""
  image_raw = tf.io.read_file(path)
  image_tensor = tf.cond(
      tf.image.is_jpeg(image_raw),
      lambda: tf.image.decode_jpeg(image_raw, channels=3),
      lambda: tf.image.decode_png(image_raw, channels=3))
  return image_tensor
  
cnt = 0
accurate_count = 0
l = []

for root, dirs, files in os.walk(src2 + 'testing'):
    for file in files:
        try:
            cnt += 1
            image_path = os.path.join(root, file)
            label = root[-1]
            image = load_image(image_path)
            image, _ = model.preprocess(image, tf.cast(int(label), tf.int64))
            image = tf.expand_dims(image, 0).numpy()
            start = datetime.datetime.now()
            interpreter.set_tensor(input_index, image)
            interpreter.invoke()
            end = datetime.datetime.now()
            prob_0 = output()[0][0]
            prob_1 = output()[0][1]
            predict_label = np.argmax(output()[0])
            predict_label_name = label_names[predict_label]
            accurate_count += (int(predict_label) == int(label))
            l.append([image_path, start, end, prob_0, prob_1, predict_label, predict_label_name, int(label)])
        except:
            print('Error for -',image_path)
    
accuracy = accurate_count * 1.0 / cnt
print('TensorFlow Lite model accuracy = %.3f' % accuracy)

df = pd.DataFrame(l,columns = ['image_path', 'start', 'end', 'prob_0', 'prob_1', 'predict_label', 'predict_label_name', 'actual_label'])
df.to_csv(src + model_name + '_eval_results.csv', index = False)